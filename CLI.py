import os
import sys
import subprocess
import zipfile
import getpass
from pathlib import Path

import mongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

from passlib.hash import sha256_crypt


def validation_overwrite_database():
    validation = input("* database already exist. \nAre you sure you want to overwrite it ? Y(yes)/N(no) : ")
    if validation == "N" or validation == "no":
        print("/!\\ creation canceled, database not overwrited")
        return False
    if validation == "Y" or validation == "yes":
        print("* database will be overwrited")
        return True
    else:
        return validation_overwrite_database()


def cubi_CLI():
    try:
        client = MongoClient('mongodb://localhost:27017/')
        client.server_info()
        subprocess.run(["sudo", "service", "mongodb", "start"])
        confirmation = False

        if len(sys.argv) == 3 and sys.argv[1] == "createcubi":
            app_name = sys.argv[2]
            if mongo.database_exist(app_name):
                confirmation = validation_overwrite_database()
            if confirmation is True or mongo.database_exist(app_name) is False:
                with zipfile.ZipFile(str(Path().absolute())+"/cubi_template.zip", 'r') as cubi_zip:
                    cubi_zip.extractall()
                print("* all files extracted to project Folder")

                os.rename("cubizip", app_name)

                username = input("Please, enter your superusername : ")
                password = getpass.getpass()
                encrypted_password = sha256_crypt.hash(password)

                db = client[app_name]
                db.drop_collection("users")
                db.create_collection("users")
                db.users.insert_one(
                    {"username": username, "password": encrypted_password, "id": 0})
                print("* created the local database : " + app_name)
                print("* admin : " + username)

                app_path = os.path.join(app_name, "settings.py")
                settings_file = open(app_path, "r")
                settings_lines = settings_file.readlines()

                settings_lines[6] = "DATABASE = '" + app_name + "'"
                settings_file = open(app_path, "w")
                settings_file.write(''.join(settings_lines))

                print("* project created, to launch the demo, go to the project folder and hint :")
                print("gunicorn -b 127.0.0.1:3000 app:app")

        if len(sys.argv) == 3 and sys.argv[1] == "backupdb":
            database = sys.argv[2]
            mongo.mongo_to_json(database, database+".db")

        if len(sys.argv) == 4 and sys.argv[1] == "restoredb":
            filename = sys.argv[2]
            database = sys.argv[3]
            if mongo.database_exist(database):
                confirmation = validation_overwrite_database()
            if confirmation is True or mongo.database_exist(database) is False:
                mongo.json_to_mongo(filename, database)

        if len(sys.argv) != 3 and len(sys.argv) != 4:
            print("usage : \n createcubi appName \n backupdb databaseName \n restoredb filename databaseName \n")

    except ConnectionFailure:
        print("connection failed")


cubi_CLI()
