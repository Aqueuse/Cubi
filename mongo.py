import sys
import json
from pathvalidate import sanitize_filename, validate_filename, ValidationError

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


def mongo_to_json(database, filename):
    try:
        client = MongoClient('localhost', 27017)
        databases_names = client.list_database_names()

        if database not in databases_names:
            print("database not found")
            return

        database_to_save = client[database]
        collections = database_to_save.collection_names()

        json_file = open(filename, 'w+')

        for collection_name in collections:
            collection = list(database_to_save[collection_name].find({}))
            if len(collection) == 0:
                print("... collection don't exist or empty \nfile not writed\n")
            else:
                collection_list = [collection_name]
                for item in collection:
                    del item['_id']  # remove objectID, because it's crap
                    collection_list.append(item)

                json_file.write(json.dumps(collection_list)+"\n")
        print("=> "+database+".db writed\n")
        json_file.close()
    except ConnectionFailure:
        print("connection failed")
        sys.exit(1)


def json_to_mongo(filename, database_name):
    try:
        client = MongoClient('localhost', 27017)
        database = client[database_name]
        mongo_to_json(database_name, 'old_'+database_name+'.db')
        print("=> saved the old data and cleaned the collection\n")

        json_file = open(filename, 'r')

        # drop all collections in the database
        for item in database.collection_names():
            database.drop_collection(item)

        collections = json_file.readlines()
        for collection in collections:
            collection_name = json.loads(collection)[0]
            collection_dict = json.loads(collection)[1:]

            database.create_collection(collection_name)

            for item in collection_dict:
                database[collection_name].insert_one(item)

        json_file.close()
        print("=> database restored  \\o/\n")
    except ConnectionFailure:
        print("connection failed")
        sys.exit(1)


def valid_filename(filename):
    try:
        validate_filename(filename)
        return filename
    except ValidationError:
        return sanitize_filename(filename)


def database_exist(app_name):
    try:
        client = MongoClient('localhost', 27017)
        dbnames = client.list_database_names()
        if app_name in dbnames:
            return True
        return False
    except ConnectionFailure:
        print("connection failed")
        sys.exit(1)
